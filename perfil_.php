<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <!--<meta http-equiv="refresh" content="5" />-->
  <title>Configuración Perfil</title>
  <link rel="stylesheet" href="./css/perfil_.css">
  <link rel="stylesheet" href="./css/fontello.css">
</head>
<body>
  <div class="content">
    <header>
      <div id="left">
        <h1 class="icon-user"></h1>
      </div>
      <div id="right">aguantapp</div>
      <div id="center">Hecho</div>
    </header>

    <div class="contentImgProfile">
      <!--<img src="assets/juan.jpg" alt="img">-->


    </div>
    <div class="content-img-profile">
      <div class="images1">flecha</div>
      <div class="images2">flecha</div>
      <div class="images3">flecha</div>
      <div class="images4">flecha</div>
    </div>
    <div class="content-informacion">
      <div class="info">
        <div class="info-usuario">
          <div class="txt1">icon</div>
          <div class="txt2">Usuario, juan, 21</div>
          <div class="txt3">buton
          </div>
        </div>
        <div class="info-ubicacion">
          <div class="txt1">icon</div>
          <div class="txt2">Ubicacion, Cali, colombia</div>
          <div class="txt3">buton
          </div>
        </div>
        <div class="info-correo">
          <div class="txt1">icon</div>
          <div class="txt2">correo, jpabadial@gmail.com</div>
          <div class="txt3">buton
          </div>
        </div>
        <div class="info-genero">
          <div class="txt1">icon</div>
          <div class="txt2">Masculino</div>
          <div class="txt3">buton
          </div>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
