<!DOCTYPE html>
<html>
<?php include("./controladores/Usuarios_Controller.php");?>
  <head>
    <meta charset="utf-8">
    <title>Chat</title>
    <script type="text/javascript" src="./sources/jQuery/jquery-3.2.0.min.js"></script>
    <script type="text/javascript" src="./sources/jQuery/jquery.lazyload.js"></script>
    <!-- <script type="text/javascript" src="./sources/touchjQuery/jquery.touchSwipe.js"></script>
    <script type="text/javascript" src="./sources/Hammer/hammer.min.js"></script> -->
    <script type="text/javascript" src="./sources/events/jquery.mobile-events.js"></script>
    <link rel="stylesheet" href="./css/stylesheets.css">
    <link rel="stylesheet" href="./css/fontello.css">
    <script type="text/javascript" src="./sources/push.min.js"></script>
    <!-- <script type="text/javascript" src="./sources/jquery.mobile-1.4.5/jquery.mobile-1.4.5.js"></script> -->
  </head>
  <body>
    <?php $usr = Usuarios_Controller::cargarPerfil();?>
    <?php foreach ($usr as $usuario):?>
    <div class="CargoUsuario" style="display:none">
      <input id="idUser" type="text" name="" value=<?php echo $usuario["id"];?>>
    </div>
  <?php endforeach;?>
  <header>
    <nav>
      <div class="chat-bloque-icon" id="maBack">
          <h1 class="icon-left-open"></h1>
      </div>
      <div class="chat-bloque-title">
            <h1>CHATGUANTAPP</h1>
      </div>
      <div class="chat-bloque-icon">

          <h1 class="notiMatch icon-cancel" >

          </h1>

      </div>
    </nav>
  </header>
    <form id="formChat">
      <div id="conversation" style="overflow-y:scroll;" class="cuerpo-mensaje">

      </div>
      <div class="div-sent">
      <div id="message" class="mensaje-enviar">
        <textarea id="inputMessage" name="message" placeholder="Escriba aqui" rows="8" cols="80"></textarea>
      </div>
      <div class="buton-msj-enviar">
        <h1 id="send" type="submit" value="Send" class="icon-forward"></h1>
        </div>
          </div>
    </form>

      <script src="./js/uxChat.js"></script>
      <script src="./js/chatJs.js"></script>
  </body>
  <!-- <script src="./js/jquery-3.1.0.min.js"></script> -->

</html>
