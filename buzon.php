<!DOCTYPE html>
<html lang="utf-8">
<?php include("./controladores/Usuarios_Controller.php");?>
<head>

  <title>Document</title>
  <script type="text/javascript" src="./sources/jQuery/jquery-3.2.0.min.js"></script>
  <script type="text/javascript" src="./sources/jQuery/jquery.lazyload.js"></script>
  <!-- <script type="text/javascript" src="./sources/touchjQuery/jquery.touchSwipe.js"></script>
  <script type="text/javascript" src="./sources/Hammer/hammer.min.js"></script> -->
  <link rel="stylesheet" href="./css/stylesheets.css">
  <link rel="stylesheet" href="./css/cabecera.css">
  <link rel="stylesheet" href="./css/fontello.css">
  <script type="text/javascript" src="./sources/events/jquery.mobile-events.js"></script>
  <script type="text/javascript" src="./sources/push.min.js"></script>
  <!-- <script type="text/javascript" src="./sources/jquery.mobile-1.4.5/jquery.mobile-1.4.5.js"></script> -->
</head>
<body>

  <?php $usr = Usuarios_Controller::cargarPerfil();?>
  <?php foreach ($usr as $usuario):?>
  <div class="CargoUsuario" style="display:none">
    <input id="idUser" type="text" name="" value=<?php echo $usuario["id"];?>>
  </div>
<?php endforeach;?>
<header>
  <nav>
    <div class="nav-profile" id="perfilConfig">
        <h1 class="icon-left"></h1>
    </div>
    <div class="nav-logo" id="logoPerfil">
          <img src="./recursos/imagenes/logo.png" alt="">
    </div>
    <div class="nav-messege" id="miBuzon">
        <h1 class="icon-paper-plane"></h1>
    </div>

  </nav>
</header>

  <div id="Contenedor" class="Cuerpo-chats">

  </div>


</body>
  <!-- <script src="./js/jquery-3.1.0.min.js"></script> -->
  <script src="./js/uxBuzon.js"></script>
  <script src="./js/buzonjs.js"></script>
</html>
