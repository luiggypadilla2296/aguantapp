creadores: Luiggy Andres Padilla
fecha: abril/24/2017

aguantApp es una aplicacion web Movil geosocial que permite a los usuarios comunicarse con
otras personas con base en sus preferencias(rango de edad, distancia y genero), esta aplicacion permite
concretar citas o encuentros con las diferentes personas presentes en la app. aguantApp fue creada en abril del 2017 por Luiggy Andres Padilla.

FUNCIONALIDADES REQUERIDAS POR EL PROFESOR:
1.versionamiento ✔
2.branches -> Luiggy,Master ✔
3.branches Luiggy actualiza a la Master ✔
4.diseño flat ✔
5.login ✔
6.registro ✔
7.sección Home, donde se encuentran las personas que no se han calificado ✔
8.configuracion donde se modifican datos del Usuario regustrado en ese momento ✔
9.Perfil de información, donde aprece la informacion adicional como distancia, nombre de las personas del home ✔
10.Perfil propio, corresponde a cada usuario registrado ✔
11.chat para conversar con los futuros matches ✔
12.La app cuanta con un menu superiro donde esta el perfil de la persona logeada, donde estan los futuros Matches y un repositorio de los chats creados en caso de haber un matches ✔
13.lazy load ✔
14.Registro de usuarios con la api de google, toma lat y log por defecto ✔
15.Seleccion de genero por parte de los usuarios registrados para encontrar futuros Matches ✔
16.las personas del Home, salen de acuerdo a loos filtros de genero, edad y distancia ✔
17.usuario que no "aguantan" ya no salen nuevamente, y los que "aguantan" salen en la seccion del chat ✔
18.El usuario en el chat podra decir que ya no aguanta ✔
19.notificacion al moento de existir un Match ✔
20.Los usuarios pueden subir n fotos y hacer cualquiera foto de perfil ✔
21.Los usuarios podran borrar cualquier imagen de su perfil y esta se eliminara del servido y de la DB ✔
22.
23.Los usuarios que tienen conpatibilidad, es decir, que ambos se "aguantan" podran chatear en tiempo real ✔
24.USO DE LAZY LOAD ✔
