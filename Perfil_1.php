<!DOCTYPE html>
<html lang="es" class="no-js">
<?php include("./controladores/Foto_Controller.php"); ?>


<head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="./sources/jquery.mobile-1.4.5/jquery.mobile-1.4.5.css">
    <script type="text/javascript" src="./sources/jQuery/jquery-3.2.0.min.js"></script>
    <script type="text/javascript" src="./sources/jQuery/jquery.lazyload.js"></script>

    <!-- <script type="text/javascript" src="./sources/jquery.mobile-1.4.5/jquery.mobile-1.4.5.js"></script> -->
    <link rel="stylesheet" href="./css/perfilStyle.css">
    <link rel="stylesheet" href="./css/fontello.css">


</head>

<body>

    <div class="wrapper">
      <header>
        <nav>
          <div class="nav-profile" id="volverAtras">
              <h1 class="icon-left"></h1>
          </div>
          <div class="nav-logo">
                <img src="./recursos/imagenes/logo.png" alt="">
          </div>
          <div class="nav-messege" id="chat">
              <h1 class="icon-paper-plane"></h1>
          </div>

        </nav>
      </header>

    </div>

    <?php $fotos= Foto_Controller::cargarFotos(); ?>


    <div class="contenidoImagenes">


    <div class="imagenIntermedia12">

    </div>
    <div class="cargarFotografias">
        <div class="contenedor">
            <div class="Galeria">
                <h2>GALERIA</h2>

            </div>
            <div class="cargarImagenes">
                <h1 class="icon-picture" id="cargadorImagenes"></h1>
            </div>

        </div>

        <div class="convoyer">
            <div class="elements">
                <?php foreach($fotos as $foto): ?>
                <div class="element itemClass" >
                    <img data-original= <?php echo '"'.'./'.$foto["archivo"].'"'; ?> alt="" class="lazyLoad" id=<?php echo $foto["id"]; ?> >
                </div>
                <?php endforeach; ?>
            </div>
        </div>

        <div class="cargaFoto">
            <div class="todo-form">
                  <div class="contenedorEmergente">
                    <div class="seleccion">
                      <form accept-charset="utf-8" method="POST" id="enviarimagenes" enctype="multipart/form-data">
                        <input type="file" name="imagen" id="file" class="inputfile inputfile-2" data-multiple-caption="{count} files selected" multiple />
                        <label for="file"><figure><img src="./recursos/imagenes/subir.png" ></figure> <span>Cargar foto&hellip;</span></label>
                        <input type="submit" class="btna" name="button" width="100px" height="100px"><!--buton  -->
                      </form>
                    </div>
                  </div>
            </div>
        </div>
        <script >
            $("#enviarimagenes").submit(function(e) {
              e.preventDefault();
              var formData = new FormData(document.getElementById("enviarimagenes"));
              $.ajax({
                type: "POST",
                dataType: "HTML",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                url: "http://localhost/aguantApp02/?controller=Foto_Controller&method=saveImagen"
              }).done(function(r) {
                console.log(r);

              });
            });

        </script>


    </div>

    </div>

    <?php $usuarios= Usuarios_Controller::cargarPerfil(); ?>
    <?php  foreach($usuarios as $usuario):?>

    <div class="opciones-menu-perfil">
      <div class="lateral-perfil">

     <div class="foto-perfil">

         <img src=<?php echo '"'.$usuario["imgProfile"].'"'; ?> alt=""></a>
       <div class="contenido-perfil">
          <div class="icono-config-perfil">
            <h1 class="icon-cog" id="configiraciones"></h1>
          </div>
          <div class="icono-imagenes-perfil">
            <h1 class="icon-picture" id="imagenes"></h1>
          </div>

          <div class="nombre-perfil">
              <h3><?php echo $usuario["edad"]; ?> Años, </h3>
              <h3><?php echo $usuario["username"]; ?></h3>
          </div>
      </div>

      </div>

        <div class="inferiorTo">
            <div class="inferior-bloque-to">
                <div class="title-bloque-to">
                  <div class="infoPersonal">
                    <h2>INFOMACIÓN PERSONAL</h2>
                  </div>
                  <div class="icon-infoPersonal">
                    <!-- <h1 class="icon-user-add" id="location"></h1> -->
                  </div>

                </div>

                <div class="imagenIntermedia">

                </div>

                <div class="about-bloque-to">
                    <div class="about-icon-bloque-to">
                        <h1 class="icon-me"></h1>
                    </div>
                    <div class="about-info-bloque-to">
                        <div class="title-info-about-to">
                            <h3>SOBRE MI</h3>
                        </div>
                        <div class="text-info-about-to">

                            <p><?php echo $usuario["descripcion"]; ?></p>
                        </div>

                    </div>

                </div>

                <div class="inferiorAbajo">
                <div class="location-bloque">
                    <div class="location-icon">
                        <h1 class="icon-location"></h1>
                    </div>
                    <div class="location-information">
                        <div class="information-title">
                            <h3>UBICACION</h3>
                        </div>
                        <div class="information-text">
                            <p><?php echo $usuario["ciudad"]; ?></p>
                        </div>
                    </div>

                </div>

                <div class="estado-bloque">
                    <div class="estado-icon">
                        <h1 class="icon-estado"></h1>
                    </div>
                    <div class="estado-information">
                        <div class="estado-title-information">
                            <h3>ESTADO</h3>
                        </div>
                        <div class="estado-text-information">
                            <p><?php echo $usuario["estado"]; ?></p>
                        </div>
                    </div>

                </div>
                </div>


            </div>

            </div>


        </div>

    </div>
    <?php endforeach; ?>

    </div>
<script src="./js/ajax.js"></script>
<script src="./Js/ux-perfil.js"></script>
<script src="js/custom-file-input.js"></script>
</body>

</html>
