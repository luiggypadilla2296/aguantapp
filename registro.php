<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title>Registro</title>
  <link rel="stylesheet" href="./css/registrarse.css">

  <link rel="stylesheet" href="./css/fontello.css">
  <script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.0.min.js"></script>

  <script type="text/javascript" src="./js/Ubication.js"></script>


</head>
<body>
  <div class="content">
    <div class="imagenLogoCarga">
      <img src="./assets/logo/logo.png" alt="imagen carga">
      <h2>aguantApp</h2>
    </div>

    <div class="inicio">
      <div class="form-wrap">

        <div class="tabs-content">
          <div id="signup-tab-content" class="active">
            <form  class="signup-form" action="" method="post">
              <div class="bloque">
                <div class="bloqueIcon">
                  <h1 class="icon-at"></h1>
                </div>
                <div class="bloqeuInput">
                  <input type="text" class="inputt" name="email" id="user_name" autocomplete="off" placeholder="email" pattern="^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$" required>
                </div>
              </div>
              <div class="bloque">
                <div class="bloqueIcon">
                  <h1 class="icon-user-o"></h1>
                </div>
                <div class="bloqeuInput">
                  <input type="text" class="inputt" name="username" id="user_name" autocomplete="off" placeholder="Username" required>
                </div>
              </div>
              <div class="bloque">
                <div class="bloqueIcon">
                  <h1 class="icon-lock"></h1>
                </div>
                <div class="bloqeuInput">
                  <input type="password" class="inputt" name="password" id="user_pass" autocomplete="off" placeholder="Contraseña" required>
                </div>
              </div>
              <div class="bloque">
                <div class="bloqueIcon">
                  <h1 class="icon-users"></h1>
                </div>
                <div class="bloqeuInput">
                  <input type="number" class="inputt" name="edad" id="user_edad" autocomplete="off" placeholder="Edad" required>
                </div>
              </div>

              <div class="bloque" style="display:none">
                <div class="bloqueIcon">
                  <h1 class="icon-key"></h1>
                </div>
                <div class="bloqeuInput">
                  <input type="text" class="inputt" name="genero" id="user_genero" autocomplete="off" placeholder="Contraseña" required value="0">
                </div>
              </div>

              <div class="bloque" style="display:none">
                <div class="bloqueIcon">
                  <h1 class="icon-key"></h1>
                </div>
                <div class="bloqeuInput">
                  <input type="text" class="inputt" name="lon" id="user_lon" autocomplete="off" placeholder="Contraseña" required>
                </div>
              </div>


              <div class="bloque" style="display:none">
                <div class="bloqueIcon">
                  <h1 class="icon-key"></h1>
                </div>
                <div class="bloqeuInput">
                  <input type="text" class="inputt" name="lat" id="user_lat" autocomplete="off" placeholder="Contraseña" required>
                </div>
              </div>

              <input type="submit" class="button" value="Sign Up" name="submitBtn" id="btn">
            </form>

            <div class="bloque">
              <div class="bloqueIcon">
                <h1 class="icon-transgender"></h1>
              </div>
              <div class="bloqeuInputt">
                <div class="input1">
                    <input type="radio" id="radio01" name="radioh" value="0"/>
                    <label for="radio01" style="font-size:40px;"><span id="manId" style="font-family:arial "></span> Hombre</label>
                </div>
                <div class="input2">
                    <input type="radio" id="radio02" name="radiom" value="0"/>
                    <label for="radio02" style="font-size:40px;"><span id="womanId" style="font-family:arial "></span> Mujer</label>
                </div>
              </div>


            </div>
            <div class="help-text">
              <p>By signing up, you agree to our</p>
              <p><a href="#">Terms of service</a></p>
            </div><!--.help-text-->
          </div><!--.signup-tab-content-->

          <!--.login-tab-content-->
        </div><!--.tabs-content-->
      </div><!--.form-wrap-->
    </div>
  </div>
<script type="text/javascript" src="./js/ajax.js"></script>
<script>
$(".input1").click(function(){document.getElementById('user_genero').value=""+1+"";});
$(".input2").click(function(){document.getElementById('user_genero').value=""+2+"";

});
</script>
  <!-- <script>
    $(".signup-form").submit(function(e) {
      e.preventDefault();
      var data = {};
      var inputs = $(this).find("input");
      $.each(inputs, function() {
        data[this.name] = this.value;
      });
      console.log(data);

      $.ajax({
        method: "POST",
        data: data,
        url: "http://localhost/aguantApp/?controller=Usuarios_Controller&method=register"
      }).done(function(r) {
        console.log(r);
        json = JSON.parse(r);
        console.log(json);
      });

    });

    </script>
    <script>
        $(".login-form").submit(function(e) {
            e.preventDefault();
            var data = {};
            var inputs = $(this).find("input");
            $.each(inputs, function() {
                data[this.name] = this.value;
            });

            $.ajax({
                method: "POST",
                data: data,
                url: "http://localhost/aguantApp/?controller=Usuarios_Controller&method=login"
            }).done(function(r) {
                console.log(r);
                json = JSON.parse(r);
                console.log(json);
                alert(json.msg);

                if (json.error == 0) {
                    document.location = "carga1_.php";
                }
            });
        });

    </script> -->

  <script type="text/javascript" src="./js/login_.js"></script>
</body>
</html>
