<!DOCTYPE html>
<html>
<?php include("./controladores/Foto_Controller.php"); ?>
<head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="./sources/jquery.mobile-1.4.5/jquery.mobile-1.4.5.css">
    <script type="text/javascript" src="./sources/jQuery/jquery-3.2.0.min.js"></script>
    <script type="text/javascript" src="./sources/jQuery/jquery.lazyload.js"></script>

    <!-- <script type="text/javascript" src="./sources/jquery.mobile-1.4.5/jquery.mobile-1.4.5.js"></script> -->
    <link rel="stylesheet" href="./css/perfil_1.css">
    <link rel="stylesheet" href="./css/fontello.css">


</head>

<body>

    <div class="wrapper">
      <header>
        <nav>
          <div class="nav-profile" id="volverAtras1">
              <h1 class="icon-left"></h1>
          </div>
          <div class="nav-logo"id="perfil">
                <img src="./recursos/imagenes/logo.png" alt="">
          </div>
          <div class="nav-messege" id="chat">
              <h1 class="icon-paper-plane"></h1>
          </div>

        </nav>
      </header>

    </div>


    <div class="opciones-menu-perfil">

<div class="perfil-persona-to">
    <div class="todo-form">
        <form class="editme">
          <div class="nombre-input">
          <div class="icon-nombre">
            <h1 class="icon-user-add"></h1>
          </div>
          <div class="nombre-input-input">
          <p>EDAD :</p>
          <input type="text" name="edad">
          <br>
          </div>
          </div>

          <div class="sobre-me-input">
          <div class="icon-sobre-me">
            <h1 class="icon-me"></h1>
          </div>
          <div class="sobre-me-input-input">
          <p>SOBRE MI:</p>
          <input type="text" name="sobremi">
          <br>
          </div>
          </div>

          <div class="estado-input">
          <div class="icon-estadoP">
            <h1 class="icon-estado"></h1>
          </div>
          <div class="estado-input-input">
          <p>ESTADO:</p>
          <input type="text" name="estado">
          <br>
          </div>
          </div>
          <div class="configura-jSlider1">


            <div class='info1'>

              <div class="icon-info1">
                <h1 class="icon-compass" id="icono"></h1>
              </div>

              <div class="SliderYtitle1">
              <div class="title-km">
                <h2>DISTACIA DE BÚSQUEDA : </h2>
              </div>

              <div class="input-jslider1">
                <input type="text" id="valueSlider1" value="0" name="DistanciaBusqueda" />
              </div>

              <div class='slider1'>
                  <div class='emptyprogress1'></div>
                  <div class='progress1'></div>
                  <div class='indicator1'></div>
              </div>
              </div>

            </div>

          </div>

          <div class="configura-jSlider2">

            <div class='info2'>

              <div class="icon-info2">
                <h1 class="icon-users" id="icono"></h1>
              </div>

              <div class="SliderYtitle2">
              <h2>MOSTRAR EDADES  : </h2>
              <input type="text" id="valueSlider2" value="0" name="mostrarEdades"/>

              <div class='slider2'>
                  <div class='emptyprogress2'></div>
                  <div class='progress2'></div>
                  <div class='indicator2'></div>
              </div>

            </div>
            </div>

          </div>

          <div class="configura-checkBox">

            <div class='infoCheck'>
              <div class="icon-check">
                <h1 class="icon-transgender " id="icono"></h1>
              </div>

              <div class= "contenido-checks">
                <h2>MUÉSTRAME : </h2>

                <div class="contenidos-input">
                    <div class="input1">
                    <input type="radio" id="radio01" name="radioh" value="1"/>
                    <label for="radio01"><span id="manId"></span>Hombre</label>
                    </div>
                    <div class="input2">
                    <input type="radio" id="radio02" name="radiom" value="0"/>
                    <label for="radio02"><span id="womanId"></span>Mujer</label>
                    </div>
                </div>
              </div>

            </div>


          </div>

          <div class="button-guardar">

                <button type="submit" class="button button2" id="btnGuardar">Guardar</button>

          </div>


        </form>
  </div>

  </div>





    </div>


    </div>
<script src="./js/ajax.js"></script>

<script src="./Js/ux-perfil_2.js"></script>
</body>

</html>
