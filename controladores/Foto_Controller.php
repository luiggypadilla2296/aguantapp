<?php
include("./entidades/Foto.php");
include("Usuarios_Controller.php");

class Foto_Controller{

public function saveImagen(){
  $archivo = $_FILES['imagen'];
  // var_dump($archivo);

  $user = Usuarios_Controller::cargarPerfil();

  foreach ($user as $usuario) {
    $foto = new Foto(null,$usuario["id"],null);

  }
  $r = $foto->subirFoto($archivo);
  $i = substr($r, 15);
  $foto->setArchivo($i);
  $f = $foto->crearFoto();

  print json_encode($f);
}

public function cargarFotos(){
  $user = Usuarios_Controller::cargarPerfil();

  foreach ($user as $usuario) {
    $foto = new Foto(null,$usuario["id"],null);

  }

  $misFotos  = $foto->cargarFotos();
  // var_dump($misFotos);

return $misFotos;
}

public function cargarFotosUsuario(){
  $user = Usuarios_Controller::cargarPerfil();
  foreach ($user as $usuario) {
    $foto = new Foto(null,$usuario["id"],null);
  }
  $misFotos  = $foto->cargarFotosUsuario();
  // var_dump($misFotos);

return $misFotos;
}

public function eliminarFotos(){
  $id = $_POST["delete"];
  $url = $_POST["url"];
  //var_dump("soy".$id);
  $user = Usuarios_Controller::cargarPerfil();
  foreach ($user as $usuario) {
    $foto = new Foto($id,$usuario["id"],$url);
  }
  //  var_dump($url);
  $i = substr($url, 16);

 unlink("..".$i);
  $misFotos  = $foto->eliminarFotos();
  print json_encode($misFotos);
}

}



?>
