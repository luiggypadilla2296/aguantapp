<?php
  include("./libs/Image.php");

class Foto {
  private $id;
  private $usuario;
  private $archivo;

  protected static $table = "foto";

  function __construct($id,$usuario,$archivo=null){
    $this->id = $id;
    $this->usuario = $usuario;
    $this->archivo = $archivo;
  }

  function getId(){
    return $this->id;
  }
  function getUsuario(){
    return $this->usuario;
  }
  function getArchivo(){
    return $this->archivo;
  }

  function setId($id){
    $this->id = $id;
  }
  function setUsuario($usuario){
    $this->usuario = $usuario;
  }
  function setArchivo($archivo){
    $this->archivo = $archivo;
  }

  public function crearFoto(){
    $db = new DataBase("localhost","root","","aguantapp");
    $values = get_object_vars($this);
    $resultado = $db->insert(self::$table,$values);
    return $resultado;

  }

  public function subirFoto($archivo){
    $db = new DataBase("localhost","root","","aguantapp");
    $i = Image::upload("../aguantApp02/img/","imagen",$this->getId());
    return($i);
    // var_dump($i);
  }


  public function subirArchivo(){
    $db = new DataBase("localhost","root","","aguantapp");
    $values = get_object_vars($this);
    var_dump($values);
    $resultado = $db->update(self::$table,$values,"id = '".$this->getId()."'");

    return $resultado;
  }

  // public function traerContenedor(){
  //   $db = new DataBase("localhost","root","","aguantapp");
  //   $traer = $db->select(self::$table,$values,"id = '".$this->getId()."'");
  //
  //
  // }

  public function cargarFotos(){
    $db = new DataBase("localhost", "root", "", "aguantapp");
    $resultado = $db->select("*",self::$table,"usuario = '".$this->getUsuario()."'");

    return $resultado;

  }

  public function cargarFotosUsuario(){
    $db = new DataBase("localhost", "root", "", "aguantapp");
    $resultado = $db->select("*",self::$table,"usuario != '".$this->getUsuario()."'");

    return $resultado;

  }
}





?>
