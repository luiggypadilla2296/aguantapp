<?php

    include("./libs/DataBase.php");
    //include("./config.php");
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Usuario
 *
 * @author pabhoz
 */

class Usuario {

    private $id;
    private $username;
    private $password;
    private $email;
    private $genero;
    private $lat;
    private $lon;
    private $radio;
    private $estado;
    private $descripcion;
    private $ciudad;
    private $edad;
    private $rangoEdad;
    private $imgProfile;

    protected static $table = "Usuario";

    function __construct($id, $username, $password, $email = null,
             $genero = null, $lat = null, $lon = null, $radio = null,$estado=null,$descripcion=null,$ciudad=null, $edad=null,$rangoEdad=null,$imgProfile=null) {
        $this->id = $id;
        $this->username = $username;
        $this->password = $password;
        $this->email = $email;
        $this->genero = $genero;
        $this->lat = $lat;
        $this->lon = $lon;
        $this->radio = $radio;
        $this->estado = $estado;
        $this->descripcion = $descripcion;
        $this->ciudad = $ciudad;
        $this->edad = $edad;
        $this->rangoEdad = $rangoEdad;
        $this->imgProfile = $imgProfile;
    }


    function getId() {
        return $this->id;
    }

    function getUsername() {
        return $this->username;
    }

    function getPassword() {
        return $this->password;
    }

    function getEmail() {
        return $this->email;
    }

    function getGenero() {
        return $this->genero;
    }

    function getLat() {
        return $this->lat;
    }

    function getLong() {
        return $this->lon;
    }

    function getRadio() {
        return $this->radio;
    }
    function getEstado() {
        return $this->estado;
    }
    function getDescripcion() {
        return $this->descripcion;
    }
    function getCiudad() {
        return $this->ciudad;
    }
    function getEdad() {
        return $this->edad;
    }
    function getRangoEdad(){
        return $this->rangoEdad;
    }
    function getImgProfile(){
        return $this->imgProfile;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setUsername($username) {
        $this->username = $username;
    }

    function setPassword($password) {
        $this->password = $password;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setGenero($genero) {
        $this->genero = $genero;
    }

    function setLat($lat) {
        $this->lat = $lat;
    }

    function setLong($lon) {
        $this->lon = $lon;
    }

    function setRadio($radio) {
        $this->radio = $radio;
    }
    function setEstado($estado) {
        $this->estado = $estado;
    }
    function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }
    function setCiudad($ciudad) {
        $this->ciudad = $ciudad;
    }
    function setEdad($edad) {
        $this->edad = $edad;
    }
    function setRangoEdad($rangoEdad){
        $this->rangoEdad = $rangoEdad;
    }
    function setImgProfile($imgProfile){
        $this->imgProfile = $imgProfile;
    }

    public function login(){

        $db = new DataBase(DB_HOST, DB_USER, DB_PASS, DB_NAME);
        $checkeo = $db->check("*", self::$table,"username = '".$this->getUsername()."' AND"
                . " password = '".$this->getPassword()."'",true);

        return $checkeo;

    }
    public function save(){

        $db = new DataBase(DB_HOST, DB_USER, DB_PASS, DB_NAME);
        $values = get_object_vars($this);
        $resultado = $db->insert(self::$table, $values);
        return $resultado;

    }

    public function mostrarUsuarios(){
        //include("../config.php");
        $db = new DataBase("localhost", "root", "", "aguantapp");
        //$db = new DataBase(DB_HOST, DB_USER, DB_PASS, DB_NAME);
        // $values = get_object_vars($this);

        $resultado = $db->select("*",self::$table,"username != '".$this->getUsername()."' AND"
                . " edad < '".$this->getRangoEdad()."'");

        return $resultado;

    }

    public function cargarPerfil(){

        $db = new DataBase("localhost", "root", "", "aguantapp");
        //$db = new DataBase(DB_HOST, DB_USER, DB_PASS, DB_NAME);
        // $values = get_object_vars($this);
        $resultado = $db->select("*",self::$table,"username = '".$this->getUsername()."'");

        return $resultado;

    }
    public function editarMe(){
      $db = new DataBase("localhost", "root", "", "aguantapp");
      //$db = new DataBase(DB_HOST, DB_USER, DB_PASS, DB_NAME);
      $values = get_object_vars($this);
      $resultado = $db->update(self::$table,$values,"username = '".$this->getUsername()."'");

      return $resultado;
    }

    function buscarNombreInvitado(){
      $db = new DataBase(DB_HOST, DB_USER, DB_PASS, DB_NAME);
      return $db->select("username", self::$table,"id = '".$this->getId()."'");
  }


}
