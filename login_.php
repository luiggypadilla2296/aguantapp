<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title>Login</title>
  <link rel="stylesheet" href="./css/login_.css">

  <link rel="stylesheet" href="./css/fontello.css">
  <!-- <script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.0.min.js"></script> -->
  <!-- <script type="text/javascript" src="./js/login_.js"></script> -->
  <script type="text/javascript" src="./sources/jQuery/jquery-3.2.0.min.js"></script>
  <script type="text/javascript" src="./sources/jQuery/jquery.lazyload.js"></script>



</head>
<body>
  <div class="content">
    <div class="imagenLogoCarga">
      <img src="./assets/logo/logo.png" alt="imagen carga">
      <h2>aguantApp</h2>
    </div>

    <div class="inicio">
      <div class="form-wrap">


        <div class="tabs-content">

          <div id="login-tab-content">
            <form class="login-form" action="" method="post">
              <div class="bloque">
                <div class="bloqueIcon">
                  <h1 class="icon-user-o"></h1>
                </div>
                <div class="bloqeuInput">
                  <input type="text" class="input"name="username" id="user_login" autocomplete="off" placeholder="Email or Username" required >
                </div>
              </div>
              <div class="bloque">
                <div class="bloqueIcon">
                  <h1 class="icon-lock"></h1>
                </div>
                <div class="bloqeuInput">
                  <input type="password" class="input" name="password" id="user_pass" autocomplete="off" placeholder="Contraseña" required>
                </div>
              </div>


              <input type="submit" class="buttonLogin" value="Login" name="signin">
            </form><!--.login-form-->
            <div class="help-text">
              <p><a href="#">Forget your password?</a></p>
            </div><!--.help-text-->
          </div><!--.login-tab-content-->
        </div><!--.tabs-content-->
      </div><!--.form-wrap-->
    </div>
  </div>
<script type="text/javascript" src="./js/ajax.js"></script>
  <!-- <script>
    $(".signup-form").submit(function(e) {
      e.preventDefault();
      var data = {};
      var inputs = $(this).find("input");
      $.each(inputs, function() {
        data[this.name] = this.value;
      });
      console.log(data);

      $.ajax({
        method: "POST",
        data: data,
        url: "http://localhost/aguantApp/?controller=Usuarios_Controller&method=register"
      }).done(function(r) {
        console.log(r);
        json = JSON.parse(r);
        console.log(json);
      });

    });

    </script>
    <script>
        $(".login-form").submit(function(e) {
            e.preventDefault();
            var data = {};
            var inputs = $(this).find("input");
            $.each(inputs, function() {
                data[this.name] = this.value;
            });

            $.ajax({
                method: "POST",
                data: data,
                url: "http://localhost/aguantApp/?controller=Usuarios_Controller&method=login"
            }).done(function(r) {
                console.log(r);
                json = JSON.parse(r);
                console.log(json);
                alert(json.msg);

                if (json.error == 0) {
                    document.location = "carga1_.php";
                }
            });
        });

    </script> -->

</body>
</html>
