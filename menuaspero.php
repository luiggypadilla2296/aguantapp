<!DOCTYPE html>
<html>

<?php include("obtenerPosicion.php");

include("./controladores/Foto_Controller.php");
include("./controladores/Aguanta_Controller.php");
?>


<head>
    <meta charset="utf-8">
    <title>Maches</title>
    <link rel="stylesheet" href="./sources/jquery.mobile-1.4.5/jquery.mobile-1.4.5.css">
    <script type="text/javascript" src="./sources/jQuery/jquery-3.2.0.min.js"></script>
    <script type="text/javascript" src="./sources/jQuery/jquery.lazyload.js"></script>
    <!-- <script type="text/javascript" src="./sources/touchjQuery/jquery.touchSwipe.js"></script>
    <script type="text/javascript" src="./sources/Hammer/hammer.min.js"></script> -->
    <script type="text/javascript" src="./sources/events/jquery.mobile-events.js"></script>
    <!-- <script type="text/javascript" src="./sources/jquery.mobile-1.4.5/jquery.mobile-1.4.5.js"></script> -->
    <link rel="stylesheet" href="./css/estilo.css">
    <link rel="stylesheet" href="./css/fontello.css">
    <link rel="stylesheet" href="./css/animation.css">


    </script>
</head>

<body>


  <div class="wrapper">

  <header>
    <nav>
      <div class="nav-profile" id="perfilConfig">
          <h1 class="icon-user-circle"></h1>
      </div>
      <div class="nav-logo">
            <img src="./recursos/imagenes/logo.png" alt="">
      </div>
      <div class="nav-messege" id="chat">
          <h1 class="icon-paper-plane"></h1>
      </div>

    </nav>
  </header>


  <?php $usuarios= Aguanta_Controller::traerMatches(); ?>
  <?php $perfiles= Usuarios_Controller::cargarPerfil(); ?>
  <?php $fotos= Foto_Controller::cargarFotosUsuario(); ?>



  <?php  foreach($usuarios as $usuario):?>
      <?php  foreach($perfiles as $perfil):?>

  <?php  $con= new Distancia();$res = $con->getDistance($perfil["lat"],$perfil["lon"], $usuario["lat"], $usuario["lon"]);?>
<!-- bloque img -->
  <?php if ($res<=$perfil["radio"]):?>

  <div class="perfil-persona" id=<?php echo $usuario["id"]; ?>>
    <div class="convoyer">
      <div class="elements">
          <?php foreach($fotos as $foto): ?>
          <?php if ($foto["usuario"]==$usuario["id"]):?>
          <div class="element itemClass" >
              <img data-original= <?php echo '"'.'./'.$foto["archivo"].'"'; ?> alt="" class="lazyLoad" id=id=<?php echo $foto["id"];?> >
          </div>
          <?php endif; ?>
          <?php endforeach; ?>
      </div>
    </div>
    <div class="hover-container">

    <div class="perfil-persona-info">
      <div class="perfil-datos">
        <div class="perfil-nombre">
          <h1> <?php echo $usuario["username"]; ?></h1>
        </div>
        <div class="perfil-edad">
            <h2><?php echo $usuario["edad"]; ?> Años</h2>
        </div>
      </div>
      <div class="perfil-ubicacion">
        <div class="ubicacion-pais">
          <div class="sector-icon">
              <h3 class="icon-location"></h3>
          </div>
          <div class="sector-bloque">
            <p>Colombia (CO)</p>
          </div>

        </div>
        <div class="ubicacion-distancia">
          <div class="ubicacion-icon">
            <h3 class="icon-distancia"></h3>
          </div>
          <div class="ubicacion-bloque">
            <?php
              echo "<p>$res/km</p>";
            ?>

        </div>
      </div>
    </div>

  </div>
  </div>

  </div>
  <div class="inferior-arriba" id=<?php echo '"'.'so'.$usuario["id"].'"'; ?>
      <div class="inferior-bloque-arriba">

          <div class="about-bloque-arriba">
              <div class="about-icon-bloque-arriba">
                  <h1 class="icon-me"></h1>
              </div>
              <div class="about-info-bloque-arriba">
                  <div class="title-info-about-arriba">
                      <h3>SOBRE MI</h3>
                  </div>
                  <div class="text-info-about-arriba">
                      <p><?php echo $usuario["descripcion"]; ?></p>
                  </div>

              </div>

          </div>
          <div class="location-bloque-arriba">
            <div class="location-icon-arriba">
              <h1 class="icon-location"></h1>
            </div>
            <div class="location-information-arriba">
              <div class="information-title-arriba">
                <h3>UBICACION</h3>
              </div>
              <div class="information-text-arriba">
                <p><?php echo $usuario["ciudad"]; ?></p>
              </div>
            </div>

          </div>

          <div class="estado-bloque-arriba">

            <div class="estado-icon-arriba">
              <h1 class="icon-estado"></h1>
            </div>
            <div class="estado-information-arriba">
              <div class="estado-title-information-arriba">
                <h3>ESTADO</h3>
              </div>
              <div class="estado-text-information-arriba">
                <p><?php echo $usuario["estado"]; ?></p>
              </div>
            </div>

          </div>

      </div>

  </div>

<?php endif;?>

<?php endforeach; ?>
<?php endforeach; ?>

  <!-- fin -->




        <div class="opciones-menu">
            <div class="superior">

                <div class="bloque" id="like">
                    <h1 class="icon-heart-empty" id="likeh1"></h1>
                </div>
                <div class="bloque" id="not">
                    <h1 class="icon-cancel" id="noth1"></h1>
                </div>

            </div>

        </div>
<!-- El dom de las imagenes -->

<!-- fin del dom -->
  <script src="./js/ux.js"></script>
  <script src="./js/checkMatch.js"></script>
</body>

</html>
